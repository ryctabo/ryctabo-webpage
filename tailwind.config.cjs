/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {
      keyframes: {
        'social-icon': {
          '0%': { transform: 'scale(1)' },
          '70%': { transform: 'scale(1.5)' },
          '100%': { transform: 'scale(1.25)' }
        }
      },
      animation: {
        'social-icon': 'social-icon 300ms ease-in-out forwards'
      }
    },
    fontFamily: {
      sans: [
        '__Inter_319',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        'Segoe UI',
        'Roboto',
        'Helvetica Neue',
        'Arial',
        'Noto Sans',
        'sans-serif',
        'Apple Color Emoji',
        'Segoe UI Emoji',
        'Segoe UI Symbol',
        'Noto Color Emoji'
      ],
      cursive: ['Borel']
    }
  },
  plugins: [
    require('tailwindcss-animated')
  ]
}
